package invoice

import "gitlab.com/rramirezlimas/composition/customer"

type Invoice struct {
	country string
	city    string
	total   float64
	client  customer.Customer
}
